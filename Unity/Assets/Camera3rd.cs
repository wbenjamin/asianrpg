using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class Camera3rd : MonoBehaviour {

    public Transform player;
    public Vector3 pivot;
    public float focalDistance;
    public Vector3 focalDirection;
    public float minDistance, maxDistance;
    public float minXRotation, maxXRotation;
    public bool invertAxis;
    public float sphereRadius;

    Vector3 cameraDirection_;
    float distance_;

    Transform mTransform;
    Camera mCamera;

    void Awake()
    {
        this.mTransform = this.transform;
        this.mCamera = this.camera;
        this.cameraDirection_ = Vector3.zero;
    }

    void Distance()
    {
        this.distance_ += Input.GetAxis("Mouse ScrollWheel");
        this.distance_ = Mathf.Min(this.maxDistance, this.distance_);
        this.distance_ = Mathf.Max(this.minDistance, this.distance_);
    }

    void Direction()
    {
        this.cameraDirection_.y += Input.GetAxis("Mouse X");
        this.cameraDirection_.y = this.cameraDirection_.y % 360;
        this.cameraDirection_.x += Input.GetAxis("Mouse Y") * (invertAxis ? -1 : 1);
        this.cameraDirection_.x = Mathf.Clamp(this.cameraDirection_.x, this.minXRotation, this.maxXRotation);
    }

    void Position()
    {
        Vector3 cameraPosition = (this.player.position + this.pivot) - (Quaternion.Euler(this.cameraDirection_) * this.player.forward) * this.distance_;
        Vector3 start = (this.player.position + this.pivot);
        Ray ray = new Ray(start, -(Quaternion.Euler(this.cameraDirection_) * this.player.forward));
        RaycastHit info;
        if (Physics.SphereCast(ray, this.sphereRadius, out info, this.distance_ - this.sphereRadius))
        {
            cameraPosition = (this.player.position + this.pivot) - (Quaternion.Euler(this.cameraDirection_) * this.player.forward) * info.distance;
        }
        this.mTransform.position = Vector3.MoveTowards(this.mTransform.position, cameraPosition, 10 * Time.deltaTime);
    }

	void LateUpdate () 
    {
        Distance();
        Direction();
        Position();
        this.mTransform.LookAt((this.player.position + this.pivot) + (Quaternion.Euler(this.cameraDirection_) * (Quaternion.Euler(this.focalDirection) *  this.player.forward)) * this.focalDistance);
	}

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.player.position + this.pivot, 0.1f);
        Gizmos.DrawWireSphere(this.transform.position, this.sphereRadius);
        Gizmos.DrawLine(
            this.player.position + this.pivot, 
            (this.player.position + this.pivot) - (Quaternion.Euler(this.cameraDirection_) * this.player.forward) * this.distance_
            );
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
            this.player.position + this.pivot,
            (this.player.position + this.pivot) + (Quaternion.Euler(this.cameraDirection_) * (Quaternion.Euler(this.focalDirection) * this.player.forward)) * this.focalDistance
            );
    }
}
