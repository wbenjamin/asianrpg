﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class MeshPainter : EditorWindow
{
    public static GameObject[] toPaint = new GameObject[1];
    public static Dictionary<GameObject, int> triangles = new Dictionary<GameObject,int>();
    public static Dictionary<GameObject, int> vertices = new Dictionary<GameObject, int>();
    public static float[] randValue = new float[1];
    public static int nbObjectGenerated = 0;
    public static int nbTriangles = 0;
    public static int nbVertices = 0;
    public static GameObject parent = null;
    public static string tag = "Untagged";
    public static float speedBrush = 0.5f;
    public static float maxScale = 1.0f;
    public static float minScale = 0.01f;
    public static float minRotY = -180.0f;
    public static float maxRotY = 180.0f;
    public static float minRotX = -180.0f;
    public static float maxRotX = 180.0f;
    public static float minRotZ = -180.0f;
    public static float maxRotZ = 180.0f;
    public static bool activated = false;

    private static MeshPainter _window = null;
    private static List<GameObject> _previousCreated = new List<GameObject>();
    private static Vector2 _scroll = Vector2.zero;
    private static float _lastDraw = 0.0f;
    private static int _arraySize = 1;
    private static int _instanceId = 0;
    private static GameObject _nextInstance = null;
    private static bool _isDrawing = false;
    private static bool _arraysFoldout = true;
    private static bool _useNormal = true;
    private static bool _control = false;

    [MenuItem("Window/Tools/MeshPainter")]
    public static void GetWindow()
    {
        MeshPainter window = GetWindow<MeshPainter>();
        window.title = "Mesh Painter";
        MeshPainter._window = window;
    }

    protected static void PaintMesh(Event ev) // Button 0 pressed paint Mesh on scene
    {
        Ray ray = HandleUtility.GUIPointToWorldRay(ev.mousePosition);
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo) == true)
        {
            if (MeshPainter._nextInstance == null)
                return;
            MeshPainter._nextInstance.transform.position = hitInfo.point;
            if (MeshPainter._useNormal == true)
                MeshPainter._nextInstance.transform.rotation = MeshPainter._nextInstance.transform.rotation * Quaternion.FromToRotation(MeshPainter._nextInstance.transform.up, hitInfo.normal);
            if (hitInfo.collider.tag != MeshPainter.tag ||
                Time.realtimeSinceStartup - MeshPainter._lastDraw < MeshPainter.speedBrush)
                return;
            MeshPainter.nbObjectGenerated += 1;
            if (MeshPainter.triangles[MeshPainter.toPaint[MeshPainter._instanceId]] == -1)
                MeshPainter.GetTrianglesVertices(MeshPainter._nextInstance, MeshPainter.toPaint[MeshPainter._instanceId]);
            MeshPainter.nbTriangles += MeshPainter.triangles[MeshPainter.toPaint[MeshPainter._instanceId]];
            MeshPainter.nbVertices += MeshPainter.vertices[MeshPainter.toPaint[MeshPainter._instanceId]];
            if (MeshPainter._window == null)
                MeshPainter.GetWindow();
            MeshPainter._window.Repaint();
            MeshPainter.InstanciateNext(hitInfo.point);
            MeshPainter._lastDraw = Time.realtimeSinceStartup;
        }
    }

    protected static void InstanciateNext(Vector3 position)
    {
        GameObject prefab = null;
        float rand = Random.Range(0.0f, 1.1f);
        float tmp = 0.0f;
        for (int i = 0, size = MeshPainter.randValue.Length; i < size; ++i)
        {
            tmp += MeshPainter.randValue[i];
            if (tmp > rand)
            {
                prefab = MeshPainter.toPaint[i];
                MeshPainter._instanceId = i;
                break;
            }
        }
        if (prefab == null)
            return;
        GameObject obj = GameObject.Instantiate(prefab, position, Quaternion.identity) as GameObject;

        if (MeshPainter.parent != null)
            obj.transform.parent = MeshPainter.parent.transform;
        //obj.transform.Rotate(90.0f, 0.0f, 0.0f);
        if (MeshPainter._useNormal == false)
            obj.transform.Rotate(Random.Range(MeshPainter.minRotX, MeshPainter.maxRotX),
                                 Random.Range(MeshPainter.minRotY, MeshPainter.maxRotY),
                                 Random.Range(MeshPainter.minRotZ, MeshPainter.maxRotZ));
        else
        {
            obj.transform.Rotate(0.0f,
                                 Random.Range(MeshPainter.minRotY, MeshPainter.maxRotY),
                                 0.0f);
        }
        float scale = Random.Range(MeshPainter.minScale, MeshPainter.maxScale);
        obj.transform.localScale = new Vector3(scale, scale, scale);
        MeshPainter._previousCreated.Add(MeshPainter._nextInstance);
        MeshPainter._nextInstance = obj;
    }

    protected static void UnDo()
    {
        if (MeshPainter._previousCreated.Count == 0)
            return;
        GameObject go = MeshPainter._previousCreated[MeshPainter._previousCreated.Count - 1];

        MeshPainter._previousCreated.Remove(go);
        DestroyImmediate(go);
    }

    public static void OnScene(SceneView view)
    {
        int controlID = GUIUtility.GetControlID(FocusType.Passive);
        Event ev = Event.current;

        //if (ev.isKey == true)
        //{
        //    if (ev.keyCode == KeyCode.Z && ev.control == true)
        //    {
        //        MeshPainter.UnDo();
        //        ev.Use();
        //    }
        //}
        if (ev.isMouse == true)
        {
            if (ev.type == EventType.MouseDown && ev.button == 0) // LEFT BUTTON DOWN
            {
                Ray ray = HandleUtility.GUIPointToWorldRay(ev.mousePosition);
                RaycastHit hitInfo;

                if (Physics.Raycast(ray, out hitInfo) == true)
                    MeshPainter.InstanciateNext(hitInfo.point);
                else
                    MeshPainter.InstanciateNext(Vector3.zero);
                MeshPainter._isDrawing = true;
                MeshPainter._lastDraw = Time.realtimeSinceStartup;
                //MeshPainter.nbObjectGenerated = 0;
                //MeshPainter.nbTriangles = 0;
                //MeshPainter.nbVertices = 0;
            }
            else if (ev.type == EventType.MouseUp && ev.button == 0) // LEFT BUTTON UP
            {
                GameObject.DestroyImmediate(MeshPainter._nextInstance);
                MeshPainter._nextInstance = null;
                MeshPainter._isDrawing = false;
            }
            if (ev.alt == true)
                return;
            else if (MeshPainter._isDrawing == true &&
                     MeshPainter.activated == true)
            {
                MeshPainter.PaintMesh(ev);
                //ev.Use();
            }
        }
        if (ev.type == EventType.Layout)
            HandleUtility.AddDefaultControl(controlID);
    }

    protected static void GetTrianglesVertices(GameObject go, GameObject key)
    {
        MeshFilter[] renderers = go.GetComponentsInChildren<MeshFilter>();
        SkinnedMeshRenderer[] skinRenderers = go.GetComponentsInChildren<SkinnedMeshRenderer>();
        int nbTriangles = 0;
        int nbVertices = 0;

        for (int i = 0, size = renderers.Length; i < size; ++i)
        {
            nbTriangles += renderers[i].sharedMesh.triangles.Length;
            nbVertices += renderers[i].sharedMesh.vertexCount;
        }
        for (int i = 0, size = skinRenderers.Length; i < size; ++i)
        {
            nbTriangles += skinRenderers[i].sharedMesh.triangles.Length;
            nbVertices += skinRenderers[i].sharedMesh.vertexCount;
        }
        MeshPainter.triangles[key] = nbTriangles;
        MeshPainter.vertices[key] = nbVertices;
    }

    void OnGUI()
    {
        GUIContent content = new GUIContent();
        int newSize = 0;
        bool active = MeshPainter.activated;

        MeshPainter._scroll = EditorGUILayout.BeginScrollView(MeshPainter._scroll, false, false);
        content.text = "Active";
        MeshPainter.activated = EditorGUILayout.Toggle(content, MeshPainter.activated);
        if (MeshPainter.activated != active)
        {
            if (active == false)
                SceneView.onSceneGUIDelegate += OnScene;
            else
                SceneView.onSceneGUIDelegate -= OnScene;
        }
        content.text = "Parent of objects in Scene";
        MeshPainter.parent = EditorGUILayout.ObjectField(content, MeshPainter.parent, typeof(GameObject), true) as GameObject;
        content.text = "Object to Paint";
        MeshPainter._arraysFoldout = EditorGUILayout.Foldout(MeshPainter._arraysFoldout, content);
        if (MeshPainter._arraysFoldout)
        {
            GameObject go;
            content.text = "Arrays size of Objects";
            newSize = EditorGUILayout.IntField(content, MeshPainter._arraySize);
            if (newSize != MeshPainter._arraySize)
            {
                int i = 0;
                GameObject[] tmp = new GameObject[newSize];
                float[] randTmp = new float[newSize];

                for (int size = Mathf.Min(newSize, MeshPainter.toPaint.Length); i < size; ++i)
                {
                    tmp[i] = MeshPainter.toPaint[i];
                    randTmp[i] = MeshPainter.randValue[i];
                }
                for (int size = randTmp.Length; i < size; ++i)
                    randTmp[i] = 0.0f;
                MeshPainter.toPaint = tmp;
                MeshPainter.randValue = randTmp;
                MeshPainter._arraySize = newSize;
            }
            for (int i = 0; i < newSize; ++i)
            {
                content.text = "Object " + i.ToString();
                GUILayout.BeginHorizontal();
                {
                    go = MeshPainter.toPaint[i];
                    MeshPainter.toPaint[i] = EditorGUILayout.ObjectField(content, MeshPainter.toPaint[i], typeof(GameObject)) as GameObject;
                    if (go != MeshPainter.toPaint[i])
                    {
                        MeshPainter.triangles.Remove(MeshPainter.toPaint[i]);
                        MeshPainter.vertices.Remove(MeshPainter.toPaint[i]);
                        if (MeshPainter.toPaint[i] != null)
                        {
                            MeshPainter.triangles[MeshPainter.toPaint[i]] = -1;
                            MeshPainter.vertices[MeshPainter.toPaint[i]] = -1;
                        }
                        //    this.GetTrianglesVertices(MeshPainter.toPaint[i]);
                    }
                    MeshPainter.randValue[i] = EditorGUILayout.FloatField((MeshPainter.randValue[i] * 100.0f)) / 100.0f;
                    content.text = "%";
                    EditorGUILayout.LabelField(content, GUILayout.MaxWidth(20));
                }
                GUILayout.EndHorizontal();
            }
        }
        content.text = "Refresh rate of brush";
        MeshPainter.speedBrush = EditorGUILayout.FloatField(content, MeshPainter.speedBrush);
        content.text = "Tag to draw on";
        MeshPainter.tag = EditorGUILayout.TagField(content, MeshPainter.tag);
        content.text = "Scale range";
        GUILayout.BeginHorizontal();
        {
            MeshPainter.minScale = EditorGUILayout.FloatField(content, MeshPainter.minScale);
            MeshPainter.maxScale = EditorGUILayout.FloatField(MeshPainter.maxScale);
        }
        GUILayout.EndHorizontal();


        content.text = "Rotation Y";
        GUILayout.BeginHorizontal();
        {
            //EditorGUILayout.MinMaxSlider(content, ref MeshPainter.minRotY, ref MeshPainter.maxRotY, -360.0f, 360.0f);
            MeshPainter.minRotY = EditorGUILayout.FloatField(content, MeshPainter.minRotY);
            MeshPainter.maxRotY = EditorGUILayout.FloatField(MeshPainter.maxRotY);
        }
        GUILayout.EndHorizontal();

        content.text = "Follow normal";
        MeshPainter._useNormal = EditorGUILayout.Toggle(content, MeshPainter._useNormal);
        if (MeshPainter._useNormal == false)
        {
            //EditorGUILayout.MinMaxSlider(content, ref MeshPainter.minScale, ref MeshPainter.maxScale, 0.01f, 5.0f);
            content.text = "Rotation X";
            GUILayout.BeginHorizontal();
            {
                //EditorGUILayout.MinMaxSlider(content, ref MeshPainter.minRotX, ref MeshPainter.maxRotX, -360.0f, 360.0f);
                MeshPainter.minRotX = EditorGUILayout.FloatField(content, MeshPainter.minRotX);
                MeshPainter.maxRotX = EditorGUILayout.FloatField(MeshPainter.maxRotX);
            }

            GUILayout.EndHorizontal();
            content.text = "Rotation Z";
            GUILayout.BeginHorizontal();
            {
                //EditorGUILayout.MinMaxSlider(content, ref MeshPainter.minRotZ, ref MeshPainter.maxRotZ, -360.0f, 360.0f);
                MeshPainter.minRotZ = EditorGUILayout.FloatField(content, MeshPainter.minRotZ);
                MeshPainter.maxRotZ = EditorGUILayout.FloatField(MeshPainter.maxRotZ);
            }
            GUILayout.EndHorizontal();
        }


        content.text = "Objects generated : " + MeshPainter.nbObjectGenerated;
        EditorGUILayout.LabelField(content);
        content.text = "Triangles generated : " + MeshPainter.nbTriangles;
        EditorGUILayout.LabelField(content);
        content.text = "Vertices generated : " + MeshPainter.nbVertices;
        EditorGUILayout.LabelField(content);

        content.text = "Reset information generated";
        if (GUILayout.Button(content) == true)
        {
            MeshPainter.nbObjectGenerated = 0;
            MeshPainter.nbTriangles = 0;
            MeshPainter.nbVertices = 0;
        }

        EditorGUILayout.EndScrollView();
    }

    public void OnDestroy()
    {
        SceneView.onSceneGUIDelegate -= OnScene;
    }
}
