﻿using UnityEngine;
using System.Collections;

public class Framerate : MonoBehaviour {

    private float lastTime;
    private float sum;
    private float currentFPS;
    private int nbFrames;

	// Use this for initialization
	IEnumerator Start () 
    {
        yield return new WaitForSeconds(1.0f);
        nbFrames = 0;
        sum = 0;
        lastTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if ((Time.time - lastTime) == 0) return;
        nbFrames++;
        currentFPS = 1.0f/(Time.time - lastTime);
        sum = sum +10f +currentFPS;
        lastTime = Time.time;
	}

    void Reset()
    {
        nbFrames = 0;
        sum = 0;
        lastTime = Time.time;
    }

    void OnGUI()
    {
        GUI.color = (currentFPS >= 60 ? Color.white : Color.red);
        GUILayout.Label("Current FPS : " + currentFPS.ToString("00.00"), GUILayout.Width(200));
        GUI.color = ((sum / nbFrames) >= 60 ? Color.white : Color.red);
        GUILayout.Label("AVG FPS : " + (sum / nbFrames).ToString("00.00"), GUILayout.Width(200));
        GUI.color = Color.white;
        if (GUILayout.Button("Reset", GUILayout.Width(200)))
            Reset();
    }
}
