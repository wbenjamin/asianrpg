using UnityEngine;
using System.Collections;

public class FreeThirdCam : MonoBehaviour {

	public bool invertY;
	public float HorizontalOffset;
	public float VerticalOffset;
	public float cameraAngle;
	public float zoomSpeed;
	public float zoomInMax;
	GameObject camRoot;
	Camera cam;
	float startTime;
	Vector3 camOffset;
	Vector3 diff;
	Vector3 lookAtDiff;
	float speed = 3;
	public float rotateSpeedX;
	public float rotateSpeedY;
	Vector3 lookAtOffset;
	Vector3 targetLookAt;
	Vector3 currentLookAt;
	//Vector3 zoomVector;
	public bool alwaysRotateCamera;
	
	
	
	void Start () {
		
		lookAtOffset = new Vector3(0,cameraAngle,0);
		
		camRoot = new GameObject ("camRoot");
		camRoot.transform.position = gameObject.transform.position;
		cam = Camera.main;
		cam.transform.position = camRoot.transform.position;
		
		CamOffset();
		
		cam.transform.parent = camRoot.transform;
		
		targetLookAt = gameObject.transform.position + lookAtOffset;
		
		currentLookAt = targetLookAt;
		
		cam.transform.LookAt(currentLookAt);		
		
		//zoomVector = new Vector3(0,0, zoomSpeed);
	}
	
	
	void CamOffset () {
		
		Vector3 newOffset = new Vector3(0, VerticalOffset, - HorizontalOffset);
		cam.transform.position = camRoot.transform.position;
		cam.transform.position += newOffset;		
		
	}
	
		
	void Update () {
								
		diff = gameObject.transform.position - camRoot.transform.position;
		
		if (diff.magnitude > 0.05f){
			
			camRoot.transform.Translate(diff * 0.85f * speed * Time.deltaTime,Space.World);
			
			//cam.transform.LookAt(gameObject.transform.position + lookAtOffset);			
			
		}
		
		targetLookAt = gameObject.transform.position + lookAtOffset;
		
		lookAtDiff = targetLookAt - currentLookAt;
		
		if (lookAtDiff.magnitude > 0.05f){
			
			currentLookAt = targetLookAt;
			currentLookAt += diff * 0.1f;	
			
		}else{
			
			currentLookAt = targetLookAt;
		}
		
		cam.transform.LookAt(currentLookAt);
		
	}
	
	void LateUpdate () {
		
		if (Input.GetMouseButton(1) || alwaysRotateCamera){
			
			float mouseX = Input.GetAxis("Mouse X");
			float mouseY = Input.GetAxis("Mouse Y");
			
			camRoot.transform.Rotate(Vector3.up, mouseX * rotateSpeedX, Space.World);
			
			if (invertY){
				
				camRoot.transform.Rotate(Vector3.right, mouseY * rotateSpeedY, Space.Self);
				
			}else{
				camRoot.transform.Rotate(Vector3.right, mouseY * - rotateSpeedY, Space.Self);
			}
			
			//cam.transform.LookAt(gameObject.transform.position + lookAtOffset);			
		}
		
		if (Input.GetAxis("Mouse ScrollWheel") > 0){
			
			Vector3 camDistance = cam.transform.position - camRoot.transform.position;
			
			if (camDistance.magnitude > zoomInMax){
			
				cam.transform.position = Vector3.MoveTowards(cam.transform.position, camRoot.transform.position, zoomSpeed);
			}
		}
		
		if (Input.GetAxis("Mouse ScrollWheel") < 0){
						
			cam.transform.position = Vector3.MoveTowards(cam.transform.position, camRoot.transform.position, -zoomSpeed);
			
		}
		
		
		camRoot.transform.position = gameObject.transform.position;
			
		cam.transform.LookAt(currentLookAt);
	}	
}
