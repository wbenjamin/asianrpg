﻿using UnityEngine;
using System.Collections;

public class FreeThirdCam2 : MonoBehaviour {
	
	public Transform target;
	
	float hPosOffset;
	public float minHPosOffset=2;
	public float maxHPosOffset=6;
	public float hPosOffsetSpeed;
	
	float vPosOffset;
	public float minVPosOffset=2;
	public float maxVPosOffset=6;
	public float vPosOffsetSpeed;
	
	public Vector3 lookAtOffset;
	
	public float kLerpPosOffset;
	public float kLerpLookAtOffset;
	
	Vector3 lookAtPos;
	
	public float angularSpeed=1f;
	float angle = 0;
	
	// Use this for initialization
	void Start () {
		hPosOffset = (minHPosOffset+maxHPosOffset)*.5f;
		vPosOffset = maxVPosOffset-.3f*(maxVPosOffset-minVPosOffset);
		
		lookAtPos = target.position+lookAtOffset;
		transform.position = cameraPosition;
	}
	
	Vector3 cameraPosition
	{
		get{
			return target.position+new Vector3(Mathf.Cos(angle)*hPosOffset,vPosOffset,Mathf.Sin(angle)*hPosOffset);
		}
	}
	
	void Update()
	{
		hPosOffset = Mathf.Clamp(hPosOffset+hPosOffsetSpeed*Input.GetAxis("Mouse ScrollWheel")*Time.deltaTime,minHPosOffset,maxHPosOffset);
		
		if (Input.GetMouseButton(1)){
			
			Debug.Log(Input.GetAxis("Mouse X"));
			angle+= Input.GetAxis("Mouse X")*angularSpeed*Time.deltaTime;
			
			vPosOffset = Mathf.Clamp(vPosOffset+Input.GetAxis("Mouse Y")*vPosOffsetSpeed*Time.deltaTime,minVPosOffset,maxVPosOffset);
		}

		
	}
	
	// Update is called once per frame
	void LateUpdate () {
	
		transform.position = Vector3.Lerp(transform.position,cameraPosition,kLerpPosOffset*Time.deltaTime);
		
		lookAtPos = Vector3.Lerp(lookAtPos,target.position+lookAtOffset,kLerpLookAtOffset*Time.deltaTime);
		transform.LookAt(lookAtPos);
	}
}
