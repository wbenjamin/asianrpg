﻿using UnityEngine;
using System.Collections;

public class DontMove : MonoBehaviour 
{
	private Vector3 __m_PositionOrigin;
	private float __m_RotationYOrigin;
	
	void Start ()
	{
		__m_PositionOrigin = this.transform.localPosition; 
		__m_RotationYOrigin = this.transform.eulerAngles.y;
	}
	
	void LateUpdate ()
	{
		this.transform.position = __m_PositionOrigin;
		this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, __m_RotationYOrigin, this.transform.eulerAngles.z);
	}
}
