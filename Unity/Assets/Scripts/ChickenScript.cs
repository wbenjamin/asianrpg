﻿using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;

public class ChickenScript : MonoBehaviour {

	void Update() {
		if(DialogueLua.GetActorField("Player", "numberPoules").AsInt == 4) {
			QuestLog.SetQuestState("Poules", QuestState.Success);
		}
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player" )//&& DialogueLua.GetActorField("Player", "SecondQuest").AsString == "Doléances-Epices")
		{
			switch(DialogueLua.GetActorField("Player", "SecondQuest").AsString) {
			case "Poules-0":
				DialogueLua.SetActorField("Player", "SecondQuest",  "Poules-1");
				DialogueManager.ShowAlert("Poules restantes: 4");
				DialogueLua.SetActorField("Player", "numberPoules",  "1");
				break;
			case "null":
				DialogueLua.SetActorField("Player", "SecondQuest",  "Poules-1");
				DialogueManager.ShowAlert("Poules restantes: 4");
				DialogueLua.SetActorField("Player", "numberPoules",  "1");
				break;
			case "Poules-1":
				DialogueLua.SetActorField("Player", "SecondQuest",  "Poules-2");
				DialogueManager.ShowAlert("Poules restantes: 3");
				DialogueLua.SetActorField("Player", "numberPoules",  "2");
				break;
			case "Poules-2":
				DialogueLua.SetActorField("Player", "SecondQuest",  "Poules-3");
				DialogueManager.ShowAlert("Poules restantes: 2");
				DialogueLua.SetActorField("Player", "numberPoules",  "3");
				break;
			case "Poules-3":
				DialogueLua.SetActorField("Player", "SecondQuest",  "Poules-4");
				DialogueManager.ShowAlert("Poules restantes: 1");
				DialogueLua.SetActorField("Player", "numberPoules",  "4");
				break;
			case "Poules-4":
				DialogueLua.SetActorField("Player", "SecondQuest",  "Poules-5");
				DialogueManager.ShowAlert("Vous avez récupérez toutes les poules");
				DialogueLua.SetActorField("Player", "numberPoules",  "5");
				break;
			}
			Destroy(this.gameObject);
		}
	}
}
