﻿using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;

public class EpicesCollision : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player" && DialogueLua.GetActorField("Player", "PrincipalQuest").AsString == "Doléances-Epices")
		{
			DialogueLua.SetActorField("Player", "PrincipalQuest",  "Doléances-Chef");
			Destroy(this.gameObject);
		}
	}
}
