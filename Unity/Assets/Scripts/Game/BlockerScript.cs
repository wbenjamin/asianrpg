﻿using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;

public class BlockerScript : MonoBehaviour {

	public string stateName;
	public string alertMessage;
	public string tagName;

	void OnTriggerEnter(Collider  other)
	{
		if (other.gameObject.tag == "Player" && DialogueLua.GetActorField("Player", "PrincipalQuest").AsString == stateName)
		{
			GameObject.Find(tagName).collider.isTrigger = true;
		} 
		else if(alertMessage != "null") {
			DialogueManager.ShowAlert(alertMessage);
		}
	}
}
