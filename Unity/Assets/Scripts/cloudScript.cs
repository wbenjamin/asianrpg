﻿using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;

public class cloudScript : MonoBehaviour {

		void Update() {
			if(DialogueLua.GetActorField("Player", "PrincipalQuest").AsString == "Doléances-Nuage") {
				this.GetComponent<MeshRenderer>().enabled = true;
				this.GetComponent<BoxCollider>().enabled = true;
			}
		}
}
