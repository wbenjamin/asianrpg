﻿using UnityEngine;
using System.Collections;

public class CharacterInteractions : MonoBehaviour 
{
	public float m_TiroSpeed = 5.0f;
	
	private bool __m_TiroInArea = false;
	private bool __m_InWater = false;
	private bool __m_OnTiro = false;
	private GameObject __m_StartTiro;
	private GameObject __m_TargetTiro;
	private float toto = 0.0f;
	
	private Vector2 __m_SkyboxStartOffset;
	private float __m_Tata = 0.0f;
	
	void Start ()
	{
	}
	
	void Update ()
	{
		if (this.CheckInput() || __m_OnTiro)
			this.DoTiro();
		else if (__m_InWater)
			Debug.Log("IN DAAA WATERRRR");
	}
	
	bool CheckInput()
	{
		if (Input.GetKeyDown(KeyCode.A) && __m_TiroInArea)
		{
			this.transform.parent.transform.GetComponent<ThirdPersonController>().enabled = false;
			this.transform.parent.transform.position = __m_StartTiro.transform.position;
			this.transform.parent.transform.LookAt(__m_TargetTiro.transform.position);
			__m_StartTiro.collider.enabled = false;
			__m_OnTiro = true;
			toto = 0.0f;
			return true;
		}
		return false;
	}
	
	void DoTiro()
	{
		this.transform.parent.transform.position = Vector3.Lerp(__m_StartTiro.transform.position, __m_TargetTiro.transform.position, toto);
		toto += 0.01f;
		//this.transform.parent.transform.Translate(this.transform.forward * Time.deltaTime * m_TiroSpeed);
		//this.transform.parent.transform.LookAt(__m_TargetTiro.transform.position);
	}
	
	void	OnTriggerEnter(Collider col)
	{
		if (col.tag == "EndTiro")
		{
			if (__m_OnTiro)
			{
				__m_OnTiro = false;
				this.transform.parent.transform.GetComponent<ThirdPersonController>().enabled = true;
				toto = 0.0f;
				return;
			}
			__m_StartTiro = col.gameObject;
			__m_TargetTiro = GameObject.FindGameObjectWithTag("StartTiro");
			__m_TiroInArea = true;
		}
		else if (col.tag == "StartTiro")
		{
			if (__m_OnTiro)
			{
				__m_OnTiro = false;
				this.transform.parent.transform.GetComponent<ThirdPersonController>().enabled = true;
				toto = 0.0f;
				return;
			}
			__m_StartTiro = col.gameObject;
			__m_TargetTiro = GameObject.FindGameObjectWithTag("EndTiro");
			__m_TiroInArea = true;
		}
		else if (col.tag == "Water")
			__m_InWater = true;
	}
	
	void	OnTriggerExit(Collider col)
	{
		if (col.tag == "EndTiro")
		{
			__m_TiroInArea = false;
			__m_StartTiro.collider.enabled = true;
		}
		else if (col.tag == "StartTiro")
		{
			__m_TiroInArea = false;
			__m_StartTiro.collider.enabled = true;
		}
		else if (col.tag == "Water")
			__m_InWater = false;
	}
}
