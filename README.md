#Asian Rpg#

## Description : ##
Unity game prototype for he hua odyssé


## Commande ##

- Press right mouse click for rotate the player camera 
- Press I on keyboard for open your inventory (you could only equipe weapon on the skin but without fight system)
- Press E on keyboard when you are near any Npc for open a dialog box and talk to him

## How to resume game prototype story ##

- talk to the first Npc you see in front of you and just take your grievances (2 quest are enabled)
* 1. Go take fruits behind the current lake where you.
2. Go forward, search the cook inside the village up on the montain, let him tell you about pirates , and after go to the holy cherry tree
3. Go to the holy cherry tree, and talk with the last npc from, if you agree with what he is talking to you he will give you acces to a cloud that will allow you to go fight pirates. (the game start when you agree with the gosht for fighting pirates).
          
## Team ##
- Samya Khemri
- André Monteiro
- Benjamin Winckell

## File organisation ##



```shell
.
├── README.md
├── Build/ : 
├── .gitignore // ignore somes specifics files to be versionned;
└── Asset/ : Build app ready to run;
    ├── Scripts/ : // contain every scripts , class by folder;
        ├── Game/ : // contain scripts use for game management;
        ├── Enemies/ : // contain folder use for game management;
            ├── Enemy1/ : // contain folder specifics enemy1 scripts;
            └── Enemy2/ : // contain folder specifics enemy2 scripts;
        └── Player/ : // contain every specifics player scripts
    ├── Images/ : // images, textures, shaders ,class by folders utility;
    ├── Scenes/ : // every scenes class by team folder;
        ├── samya/ : // development scene of samya, she could make modification on her scene;
        ├── andré/ : // development scene andré, he could make modification on his scene;
        ├── benjamin/ : // development scene benjamin, he could make modification on his scene;
        ├── Preprod/ : // scene test for merge every modifications made on our own scenes;
        └── Prod/ : // WORKING SCENE only benjamin will make updates from preprodScene to this one;
    └── Prefabs/ : // contain every games prefabs class by folders utility;
        ├── samya/ : // development folder samya, she could make modification on her folder;
        ├── andré/ : // development folder andré, he could make modification on his folder;
        ├── benjamin/ : // development folder benjamin, he could make modification on his folder;
        ├── Preprod/ : // prefab folder for merge every modifications made on our own scenes;
        └── Prod/ : // WORKING folder only benjamin will make updates from preprodScene to this one;

```

## How to work With other Team Mates ##

1. Git clone of this repository 'git@bitbucket.org:wbenjamin/asianrpg.git'.

2. Create a new Branch for feature you want to work on like that: `for ben it could be BW_#numberOfbranchYouCreated_player_gamepad_control`.

3. Open your repository from unity inside a new project

4. Create your own scene if it does'nt exist by copy prod scene to you /Scenes/yourname.

5. Work inside your own scene.

6. If you want prefabs just create your own from  Prefabs/Prod/ if prefab you want already exist, else create your inside Prefabs/yourname/utility/

7. When you finish to work on a specific feature make a pull request on git bucket webSite.
``` button :  Crée une Demande D'ajout```

8. When you finish a feature, start a new branch with the name of feature your will work on.


### Git use ###
- ALWAYS create branch from master when you start to work on something
- When you make a pull request : `Crée une Demande d'ajout` stop working on this branch and wait, it will be mergin fast on preprod and prod. when it will be done replace scene inside your dev directory (where your dev scene is) by prod scene.